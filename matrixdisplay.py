#!/usr/bin/python
#import sys

#from RPLCD import CharLCD

#lcd = CharLCD(cols=16, rows=2, pin_rs=13, pin_e=11, pins_data=[12, 15, 16, 18])

#def write_to_lcd(hum, temp):
#	lcd.cursor_pos = (0, 0)
#	lcd.write_string("HELLO")
#	lcd.write_string("Temp: %dC Hum:%d%%" % temp,hum)
#	lcd.cursor_pos = (1, 0)
#	lcd.write_string("Distance: %d cm" % distance)


#!/usr/bin/python
import sys
import RPi.GPIO as GPIO

from RPLCD import CharLCD

#GPIO.setwarnings(False)

lcd = CharLCD(cols=16, rows=2, pin_rs=13, pin_e=11, pins_data=[12, 15, 16, 18])

def write_to_lcd(hum, temp, distance):
	GPIO.setmode(GPIO.BOARD)
	lcd.cursor_pos = (0, 0)
	lcd.write_string("T:%d C H: %d %%" % (temp,hum))
	lcd.cursor_pos = (1, 0)
	lcd.write_string("Distance: %d cm" % distance)

if __name__ == '__main__':
	lcd.cursor_pos = (0,0)
	lcd.write_string("HELLO")
	lcd.cursor_pos = (1,0)
	lcd.write_string("WORLD")
