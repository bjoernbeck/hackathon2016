#! /usr/bin/python

import dht11
import range_sensor as rs
import matrixdisplay as lcd
import counter
import RPi.GPIO as GPIO
import datetime, time
import MySQLdb as sql

db = 0
delay_time = 7

def connect_to_database():
	db = sql.connect(
		host="localhost",
		user="root",
		passwd="raspberry")

	cur = db.cursor()

	# check if 'smarthome' database exists, else create
	if cur.execute("SELECT * FROM information_schema.schemata WHERE schema_name like 'smarthome'") != 1:
		cur.execute("CREATE DATABASE smarthome")

	cur.execute("USE smarthome")

	# create new table each month
	year = datetime.datetime.now().strftime('%Y')
	month = datetime.datetime.now().strftime('%B').lower()
	if cur.execute("SELECT * FROM information_schema.tables WHERE TABLE_SCHEMA LIKE 'smarthome' AND TABLE_NAME LIKE '"+year+"_"+month+"'") != 1:
		cur.execute("CREATE TABLE "+year+"_"+month+"(timestamp timestamp, temperature DECIMAL(3,1), humidity DECIMAL(3,1))")

	return cur

def insert_data_into_database(cursor,hum,temp):
	tablename = str(datetime.datetime.now().strftime('%Y'))+"_"+str(datetime.datetime.now().strftime('%B')).lower()
	cursor.execute("INSERT INTO "+tablename+" VALUES (%s,%0.1f,%0.1f);",(datetime.datetime.now(),temp,hum))

if __name__ == '__main__':
	counter.setup()
	while True:
		counter.count(delay_time)
		hum,temp = dht11.get_data()
		distance = rs.get_data()
		print "Temperature:",temp,"- Humidity:",hum,"- Distance:",distance
		lcd.write_to_lcd(hum,temp,distance)
#		time.sleep(delay_time)

GPIO.cleanup()
