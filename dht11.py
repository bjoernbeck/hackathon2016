#! /usr/bin/python

import sys
import Adafruit_DHT as DHT

humidity, temp = DHT.read_retry(11,4)

if __name__ == '__main__':
	if len(sys.argv) == 2:
		if sys.argv[1]=='-r' or sys.argv[1]=='--raw':
			print humidity,temp
			sys.exit(0)
		elif sys.argv[1]=='-t' or sys.argv[1]=='--temp':
			print '{0:0.1f}'.format(temp)
			sys.exit(0)
		elif sys.argv[1]=='-h' or sys.argv[1]=='--hum':
			print '{0:0.1f}'.format(humidity)
			sys.exit(0)
		else:
			print 'Wrong commandline argument'
			sys.exit(1)

	elif len(sys.argv) > 2:
		print 'Too many commandline arguments'
		sys.exit(2)		
	else:
		print 'Temperature: {0:0.1f} C'.format(temp)
		print 'Humidity: {0:0.1f} %'.format(humidity)
		sys.exit(0)

def get_data():
	humidity, temp = DHT.read_retry(11,4)
	return humidity,temp
